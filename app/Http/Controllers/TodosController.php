<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Todo;

class TodosController extends Controller
{
    public function index()
    {
        $todos = Todo::all();
        return view('todos')->with('todos',$todos);
    }

    public function store(Request $request)
    {
        //dd($request->all());

        
        $todo = new Todo;

        $todo->todo = $request->todo;
        $todo->save();

        Session::flash('success', 'Your Todo Create Successfully.');
        return redirect()->back();
    }

    public function delete($id)
    {
       // dd($id);
       $todo = Todo::find($id);
       $todo->delete();

       Session::flash('success', 'Your Todo Delete Successfully.');
       return redirect()->back();
    }

    public function update($id)
    {
       $data['todos'] = Todo::find($id);
       return view('update',$data);
    }

    public function save(Request $request, $id)
    {
        //dd($request->all());
        
        $todo = Todo::find($id);

        $todo->todo = $request->todo;
        $todo->save();

        Session::flash('success', 'Your Todo Update Successfully.');
        return redirect()->route('todos');
    }

    public function completed($id)
    {
        //dd($request->all());
        
        $todo = Todo::find($id);

        $todo->completed = 1;
        $todo->save();

        Session::flash('success', 'Your Todo Mark as Completed.');
        return redirect()->back();
    }
}
