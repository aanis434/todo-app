@extends('layout')

@section('content')

    
    
    <div class="row">
       <div class="col-lg-8 col-lg-offset-2">
        <form action="/create/todo" method="post">
            {{ csrf_field() }}
            <input type="text" class="form-control input-lg" name="todo" placeholder="Create Todo list">
        </form>
       </div>
    </div>
    <hr>
        @foreach($todos as $todo)
             {{$todo->todo}} 
             <sup>
                 <a href="{{ route('todo.update',['id'=>$todo->id])}}" class="btn btn-sm btn-warning">Update</a> 
                 @if(!$todo->completed)
                    <a href="{{ route('todo.completed',['id'=>$todo->id])}}" class="btn btn-sm btn-info">Mark as Completed</a> 
                @else
                    <span class="btn btn-sm btn-success">Completed</span>
                @endif
                <a href="{{ route('todo.delete',['id'=>$todo->id])}}" class="btn btn-sm btn-danger">x</a> 
            </sup> 
            <hr>
        @endforeach    

@stop